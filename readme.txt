DONE algorithm version 1.0
Matlab version

Laurens Bliek & Hans Verstraete, 2015

DONE minimizes a function that may suffer from noise, by approximating the function with a random Fourier expansion.

To use:
-Click 'Downloads' on the left
-Click 'Download Repository'
-Unzip the downloaded file

Alternatively:
-Click 'Source' on the left
-Click 'DONE.m'
-Copy the code to an empty 'DONE.m' file on your computer

Type 'help DONE' in Matlab command line for help.

Please contact l.bliek@tudelft.nl or h.r.g.w.verstraete@tudelft.nl for questions and comments.